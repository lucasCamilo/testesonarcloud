var valor_total = 0,
    qtd = 1, //qtd de bebida
    total_bebida = 0,
    valor_marmita = 0,
    qtd_marmitas = [],
    marmitas = [],
    bebida = [],
    TOTAL = document.getElementById("valor_total"),
    sobremesa = document.querySelector("#sobremesa"),
    j= 0,
    valor_sobremesa = 0,
    PEDIDO_FINAL = [];

sobremesa.addEventListener( 'change', function() { // função para adicionar valor da sobremesa no total
    if(this.checked) {
        valor_sobremesa = 2
    } else {
        valor_sobremesa = 0
    }
})

// ---------------------------------------------------------------FUNÇÕES MARMITA
function verificar(n) { // verifica a quantidade carne
    var CheckMax = n, // vai receber a quantidade de carnes de cada tamanho da marmita
        carnes = [document.getElementById("frango"), document.getElementById("carne"), 
                document.getElementById("carne-porco")],
        qtd_carnes = [], //vai receber as carnes selecionadas do usuário
        contador = 0;

    for (i=0;i<carnes.length;i++) {
            if (contador>CheckMax) { //verifica se o número de carnes ultrapassou o permitido
                break
            }
            if (carnes[i].checked) { //verifica se determinada carne foi selecionada
                contador++
                qtd_carnes[i] = carnes[i].value // adiciona a carne no array
            }
    }
    var carnes_selecionadas = qtd_carnes.filter(function (i) { // apaga elementos null, 0 e indefined no qtd_carnes
        return i;
    })
    return carnes_selecionadas
}

function adicionar(){ // adiciona marmita na lista
    var tam_p = document.getElementById("tam-p"),
        tam_m = document.getElementById("tam-m"),
        tam_g = document.getElementById("tam-g"),
        comidas = document.getElementsByName("comidas-itens"),
        comidas_selecionadas = [],
        lista = document.getElementById("lista"),
        li = document.createElement("li"),
        tamanho = "",
        completo = document.getElementById("completo");

    if (tam_p.checked || tam_m.checked || tam_g.checked) {
        if (tam_p.checked) {
            carnes = verificar(0)
            tamanho = ["PEQUENA"]
            valor_marmita += 12 + valor_sobremesa
            valor_total = valor_marmita + total_bebida
            TOTAL.innerHTML = `R$ ${valor_total}`

        } else if (tam_m.checked) {
            carnes = verificar(1)
            tamanho = ["MÉDIA"]
            valor_marmita += 14 + valor_sobremesa
            valor_total = valor_marmita + total_bebida
            TOTAL.innerHTML = `R$ ${valor_total}`
        } else if (tam_g.checked) {
            carnes = verificar(2)
            tamanho = ["GRANDE"]
            valor_marmita += 16 + valor_sobremesa
            valor_total = valor_marmita + total_bebida
            TOTAL.innerHTML = `R$ ${valor_total}`

        }
    } else {
        window.alert("SELECIONE UM TAMANHO DE MARMITA")
    }

    for (i=0;i<comidas.length;i++) { //Verifica quais comidas foram selecionadas
        if (comidas[i].checked) {
            comidas_selecionadas[i] = comidas[i].value
        }
    }

    comidas_selecionadas = comidas_selecionadas.filter(function (i) { //SELEÇÃO DE TODOS OS ALIMENTOS DO USUÁRIO
        return i;
    }).concat(carnes)

    if (completo.checked) { // Verifica se a opção completa foi marcada e substitui as comidas por uma palavra só
        comidas_selecionadas = "COMPLETA - "
        comidas_selecionadas = comidas_selecionadas.concat(carnes)
    }
    
    if (valor_sobremesa == 2) {
        var sobremesa = ["sobremesa"]
        comidas_selecionadas = comidas_selecionadas.concat(sobremesa)
        li.innerHTML = `<strong>MARMITA ${tamanho} C/ SOBREMESA</strong><br> ${comidas_selecionadas}`
        lista.appendChild(li)
    } else { 
        li.innerHTML = `<strong>MARMITA ${tamanho}</strong><br> ${comidas_selecionadas}`
        lista.appendChild(li)
    }
    marmitas = tamanho.concat(comidas_selecionadas)
     if (j<15) {
        qtd_marmitas[j] = marmitas
    }
    j++
}

function selecionado(x, y) { // muda cor das comidas selecionadas
    var comida = document.getElementById(y)
    
    if (comida.checked) {
        document.getElementById(x).style.backgroundColor="rgb(195, 162, 122)"
    } else {
        document.getElementById(x).style.backgroundColor="rgba(255, 228, 196, 0.363)"
    }
}

function completo() { // marmita completa automática
    var completo = document.getElementById("completo")
    if (completo.checked) {
        document.getElementById('12').style.backgroundColor="rgb(78, 136, 56)"
    } else {
        document.getElementById('12').style.backgroundColor="rgba(164, 255, 130, 0.37)"
    }
}
// ---------------------------------------------------------------//FUNÇÕES MARMITA

// ---------------------------------------------------------------FUNÇÕES BEBIDAS
function bebidas(x) { //lista de bebidas
    qtd=1
    var preço_bebida = parseInt(document.getElementById("lista_bebida").value),
        valor_bebida = 0,
        qtd_bebida = document.getElementById("qtd"),
        display = document.getElementById(x).style.visibility;

    if (preço_bebida == 1 || preço_bebida == 2) {
        valor_bebida = 3;
        bebida = ["água sem gás"]
        if (preço_bebida == 2) {
            bebida = ["água com gás"]
        }
    } else if (preço_bebida == 3 || preço_bebida == 4) {
        valor_bebida = 4;
        bebida = ["coca lata"]
        if (preço_bebida == 4) {
            bebida = ["guaraná lata"]
        }
    } else if (preço_bebida == 5 || preço_bebida == 6) {
        valor_bebida = 6;
        bebida = ["coca 600ml"]
        if (preço_bebida == 6) {
            bebida = ["guaraná 600ml"]
        }
    } else if (preço_bebida == 7 || preço_bebida == 8) {
        valor_bebida = 10;
        bebida = ["coca 1,5L"]
        if (preço_bebida == 8) {
            bebida = ["kuat 2L"]
        }
    }

    if (display = "hidden") {
        document.getElementById(x).style.visibility = 'visible'
    }
    qtd_bebida.innerHTML = qtd
    total_bebida = valor_bebida
    valor_total = total_bebida + valor_marmita
    TOTAL.innerHTML = `R$ ${valor_total}`
}

function add_bebida() { //função do botão adicionar bebida
    var add_bebida = document.getElementById("qtd"),
        preço_bebida = parseInt(document.getElementById("lista_bebida").value),
        valor_bebida = 0;

        if (preço_bebida == 1 || preço_bebida == 2) {
            valor_bebida = 3;
        } else if (preço_bebida == 3 || preço_bebida == 4) {
            valor_bebida = 4;
        } else if (preço_bebida == 5 || preço_bebida == 6) {
            valor_bebida = 6;
        } else if (preço_bebida == 7 || preço_bebida == 8) {
            valor_bebida = 10;
        }

    qtd++
    add_bebida.innerHTML = qtd
    total_bebida = total_bebida + valor_bebida
    valor_total = total_bebida + valor_marmita
    TOTAL.innerHTML = `R$ ${valor_total}`
}

function dim_bebida() { //função do botão diminuir bebida
    if (qtd > 0) {
        var add_bebida = document.getElementById("qtd"),
            preço_bebida = parseInt(document.getElementById("lista_bebida").value),
            valor_bebida = 0;

            if (preço_bebida == 1 || preço_bebida == 2) {
                valor_bebida = 3;
            } else if (preço_bebida == 3 || preço_bebida == 4) {
                valor_bebida = 4;
            } else if (preço_bebida == 5 || preço_bebida == 6) {
                valor_bebida = 6;
            } else if (preço_bebida == 7 || preço_bebida == 8) {
                valor_bebida = 10;
            }

        qtd--
        add_bebida.innerHTML = qtd
        total_bebida = total_bebida - valor_bebida
        valor_total = total_bebida + valor_marmita
        TOTAL.innerHTML = `R$ ${valor_total}`
    }
}
// ---------------------------------------------------------------//FUNÇÕES BEBIDAS

// ---------------------------------------------------------------POPUP CONFIRMAR
function confirmar(n) {
    var form_pag = document.getElementsByName("form_pag"),
        pagamento = "",
        endereço = [nome = document.getElementById("form_nome").value,
                    contato = document.getElementById("form_fone").value, 
                    rua = document.getElementById("form_end").value,
                    n_rua = document.getElementById("form_num_end").value],
        obs = document.getElementById("obs").value,
        confirmação = document.getElementById("pedido_marmita"),
        display = document.getElementById(n).style.visibility;

    form_pag[0].checked ? pagamento = form_pag[0].value :
    form_pag[1].checked ? pagamento = form_pag[1].value :
    form_pag[2].checked ? pagamento = form_pag[2].value : window.alert("SELECIONE UMA FORMA DE PAGAMENTO")

    PEDIDO_FINAL = [endereço, pagamento, qtd_marmitas, bebida, valor_total, obs]
    console.log(PEDIDO_FINAL)
    confirmação.innerHTML = `<strong>${endereço[0]}</strong> -- ${endereço[1]}<br><br><strong>ENDEREÇO: </strong>${endereço[2]}, ${endereço[3]}
                            <br> <strong>PAGAMENTO:</strong> ${pagamento}<br><br> <strong>PEDIDO:</strong><br>${qtd_marmitas.length} marmita(s)<br>${bebida}<br><br><br>
                            VALOR TOTAL: R$${valor_total},00<br><br> OBS: ${obs}`

    
    if (display = "hidden") {
        document.getElementById(n).style.visibility = 'visible'
        document.documentElement.scrollBy(0, 1000)
    }
}

function enviar() {
    window.location.href="confirmado.html"
}
// ---------------------------------------------------------------//POPUP CONFIRMAR

